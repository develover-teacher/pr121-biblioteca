# pr121-biblioteca

Práctica d'Introducció a Gitlab amb Java i Netbeans. Crear el model de dades d'una biblioteca de forma colaborativa.

### ejinterfaces
Ejemplo de clases que implementan los métodos de una sola interfaz.

### ejinterfacesmultiples
Ejemplo de clases que implementan métodos de múltiples interfaces.

### biblioteca
Ejemplo del modelo de una biblioteca; con clases y herencia.
