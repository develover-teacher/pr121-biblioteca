
import java.io.IOException;
import java.util.Scanner;

public class Practica_122 {

    static void menu() {
        System.out.println("1.-Donar la volta\n"
                + "2.- Trobar la base més repetida,\n"
                + "3.- Trobar la base menys repetida\n"
                + "4.- Fer recompte de bases\n"
                + "5.- Sortir.");
    }

    public static void main(String[] args) throws IOException {

        menu();
        Scanner teclat = new Scanner(System.in);
        System.out.println("Enter a option:");
        int option = teclat.nextInt();

        String ruta = "/home/tarda/";
        String nameFile = "archivo.txt";

        ADN_FileReader file = new ADN_FileReader();

        file.muestraContenido(ruta + nameFile);

    }

}
