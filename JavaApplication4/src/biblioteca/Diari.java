
package biblioteca;

import java.util.Date;


public class Diari extends Publicacio{
    
    //attributes
    private String type;
    private String language;
    private int numPages;
    
    
    //constructor
    public Diari(String type, String language, int numPages, String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
        this.type = type;
        this.language = language;
        this.numPages = numPages;
    }
    
    //getters and setters
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }

    @Override
    public String toString() {
        return "Diari{" + "type=" + type + ", language=" + language + ", numPages=" + numPages + '}';
    }
}
