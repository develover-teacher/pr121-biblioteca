package biblioteca;

import java.util.Date;
/**
 *
 * @author tarda
 */
public class ebook extends Llibre{
    
    //Atributs:
    private String marca;
    private boolean bateria = false;
    private Llibre[] listOfBooks;

    
    
    
    public ebook(String codi, String nom, String editorial, Date data, String autor, int edicio, int ISBN, String marca, boolean bateria, Llibre[] listOfBooks) {
        super(codi, nom, editorial, data, autor, edicio, ISBN);
        this.bateria = bateria;
        this.listOfBooks = listOfBooks;
        this.marca = marca;
        
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public boolean isBateria() {
        return bateria;
    }

    public void setBateria(boolean bateria) {
        this.bateria = bateria;
    }

    public Llibre[] getListOfBooks() {
        return listOfBooks;
    }

    public void setListOfBooks(Llibre[] listOfBooks) {
        this.listOfBooks = listOfBooks;
    }

    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }
    
    public void royWasHere(){
        System.out.println("Roy was here");
    }
    
    @Override
    public String toString() {
        String emarca = "[ Marca: ";
        String ebateria = " Bateria: ";
        String ellibres = " Llista: ";
        String fi = " ].";
        return emarca + marca.toString() + ebateria + bateria + ellibres + listOfBooks.toString();
    }
}
