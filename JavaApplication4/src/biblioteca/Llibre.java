package biblioteca;

import java.util.Date;

/**
 * @author Miquel Àngel Amorós Alberich
 */
public class Llibre extends Publicacio
{
// Camps adicionals propis d'un llibre

  private String autor;
  private int edicio;
  private int ISBN;

// Constructor del tipus llibre. 

  public Llibre (String codi, String nom, String editorial, Date data, String autor, int edicio, int ISBN) 
  {
	super(codi,nom,editorial,data);
	this.autor = autor;
	this.edicio = edicio;
	this.ISBN = ISBN;
  }

// M�todes per a agafar els camps adicionals del llibre

  public void setAutor(String autor) {
	this.autor = autor;
  }

  public String getAutor() {
	return autor;
  }

  public void setEdicio(int edicio) {
	this.edicio = edicio;
  }

  public int getEdicio() {
	return edicio;
  }

  public void setISBN(int ISBN) {
	this.ISBN = ISBN;
  }

  public int getISBN() {
	return ISBN;
  }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Llibre{autor=");
        sb.append(autor);
        return sb.toString();
        // return "Llibre{" + "autor=" + autor + ", edicio=" + edicio + ", ISBN=" + ISBN + '}';
    }
   
   
}
