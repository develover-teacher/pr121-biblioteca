package biblioteca;

import java.util.*;
/**
 * Classe que crea menis textuals interactius; on es mostren les 
 * possibles opcions per pantalla i l'usuari elegeix la opció per teclat.
 * @author Miquel Àngel Amorós Alberich.
*/
public class Menu 
{
	List<String> menu = new ArrayList<String>();
	boolean titolMenu;
        
	public Menu (String[] menu) 
	{
		for (int i = 0; i < menu.length; i++)
		{
			this.menu.add(menu[i]); 
		}
	}
	
        /**
         * Mostra la llista d'opcions del menú que s'han inserit al constructor Menu.
         */
        public void mostraMenuPantalla() {
            	System.out.println("TRIA UNA DE LES SEGÜENTS OPCIONS:");
		int numOp = 1;
                int NUM_ELEMS = menu.size();
                for (String opMenu : menu) {
                    System.out.println(numOp + " -> " + opMenu);
                    numOp++;
                }
		System.out.println("Introdueix un número entre el 1 i " + (NUM_ELEMS));
        }
        
        /**
         * Mostra la llista d'opcions passades pel constructor i es demana a l'usuari
         * que n'insereixi una per teclat, i es verifiqui si està dins de l'intèrval 
         * entre 1 i el numero d'opcions existent.
         * @return opcio Opció de menú triada.
         */
	public int triaOpcioMenu() 
	{
                mostraMenuPantalla();
                int NUM_ELEMS = menu.size();
		Scanner teclat = new Scanner(System.in);
			
		String op = teclat.nextLine();
		int opcio = Integer.parseInt(op);
		while ((opcio < 1) || (opcio > NUM_ELEMS))
		{
			System.out.println("Opció no vàlida. Has d'introduir un numero entre 1 i " + (NUM_ELEMS));
			op = teclat.nextLine();
			opcio = Integer.parseInt(op);
		} 
		System.out.println();
		return opcio;	
	}
}
