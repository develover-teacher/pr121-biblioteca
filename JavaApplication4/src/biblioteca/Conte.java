package biblioteca;

import java.util.Date;

public class Conte extends Publicacio {

    private String category;
    private int num_paginas;
    // private String 

    public Conte(String category, int num_paginas, String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
        this.category = category;
        this.num_paginas = num_paginas;
    }

    public Conte(String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getNum_paginas() {
        return num_paginas;
    }

    public void setNum_paginas(int num_paginas) {
        if(num_paginas>200){
            System.out.println("Un libro con demasiadas paginas");
        }else{
            System.out.println("Un buen libro. Corto");
        }
            
        this.num_paginas = num_paginas;
    }

    @Override
    public String toString() {
        return "Conte{" + "category=" + category + "num_paginas="+num_paginas+'}';
    }
    

}
