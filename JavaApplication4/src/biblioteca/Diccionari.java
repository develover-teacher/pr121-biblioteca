/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;


/**
 *
 * @author tarda
 */
public class Diccionari extends Publicacio {
    
    private String idioma;
    private int edicio;
    private int ISBN;
    
    public Diccionari (String codi, String nom, String editorial, Date data, String idioma, int edicio, int ISBN){
        
        super(codi,nom,editorial,data);
        this.idioma = idioma;
        this.edicio = edicio;
        this.ISBN = ISBN;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getEdicio() {
        return edicio;
    }

    public void setEdicio(int edicio) {
        this.edicio = edicio;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    @Override
    public String toString() {
        return "Diccionari{" + "idioma=" + idioma + ", edicio=" + edicio + ", ISBN=" + ISBN + '}';
    }
}
