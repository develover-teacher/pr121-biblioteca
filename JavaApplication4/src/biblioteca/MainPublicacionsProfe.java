package biblioteca;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Prova de com fer una classe que insereixi publicacions en una
 * coleccio i les msotri per pantalla.
 * @author profe M015
 */
public class MainPublicacionsProfe {
    
    static Date definirDate(String dataDDMMAAA) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.parse(dataDDMMAAA);
    }
    
    static String[] menuProva = {
	// "ET TROBES AL MENÚ PRINCIPAL DEL ADMINISTRADOR.",
	"Llistar publicacions.",
	"Sortir."
    };
    
    static Scanner teclat = new Scanner(System.in);
       
    public static void main(String[] args) throws ParseException {
                
	Menu menu = new Menu(menuProva);
        ColPublicacions publicacions = new ColPublicacions();
        boolean dentroMenu = true;
        Publicacio pubNova; 
        
        // Fill collection with data.
        // Crear una fecha en Java 
        // https://guru99.es/java-date/
        publicacions.creaPublicacio
            (new Publicacio("P-PubDemo",
                "Publicacio Demo","EdDemo",definirDate("23/11/2019")));
         publicacions.creaPublicacio
            (new Publicacio("P-FeminismeButxaca",
                "Feminisme de Butxaca","Angle Editorial",definirDate("08/03/2017")));
                 publicacions.creaPublicacio
            (new Publicacio("P-NoLogo",
                "No Logo","Naomi Klein",definirDate("13/05/2003")));                 
        
        while(dentroMenu) {   
            menu.mostraMenuPantalla();
            int opcioMenu = menu.triaOpcioMenu();  
            switch (opcioMenu) {
                case 1:
                    System.out.println("Llistar publicacions.");
                    publicacions.llistaPublicacions();
                    break;
                
                case 2:
                    dentroMenu=false;
                    break;
            }
        }
        
    }
}
