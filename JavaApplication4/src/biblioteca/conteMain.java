/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import static biblioteca.MainPublicacionsProfe.definirDate;
import static biblioteca.ProgramaPublicacions.menuProva;
import static biblioteca.ProgramaPublicacions.teclat;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

//crear contes en un arralyst y HaspMAP
public class conteMain {

    static List<Conte> contesArraylist = new ArrayList<>();
    static Map<String, Conte> contesMap = new HashMap<String, Conte>();

    static Date definirDate(String dataDDMMAAA) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.parse(dataDDMMAAA);
    }

    public static void main(String[] args) throws ParseException {

        Scanner teclat = new Scanner(System.in);

        int option;
        do {
            System.out.println("1.-Vols afegir un Conte a un Arralist");
            System.out.println("2.-Vols afegir un Conte a un HasMap");
            System.out.println("3.-Exit");
            System.out.println("Escull una opció :");
            option = teclat.nextInt();
            //se tendría  que validar 
            switch (option) {
                case 1:
                    System.out.println(" Conte to Arralyst");
                    //Conte(String category, int num_paginas, String codi, String nom, String editorial, Date data)
                    System.out.println("Digues la categoria:");
                    String category = teclat.nextLine();
                    System.out.println("Digues el numero de págines:");
                    int num = teclat.nextInt();
                    System.out.println("Digues el codi:");
                    String codi = teclat.nextLine();
                    System.out.println("Digues el nom:");
                    String nom = teclat.nextLine();
                    System.out.println("Digues la editorial:");
                    String editorial = teclat.nextLine();
                    //System.out.println("DIgues la data de publicació:");
                    //String fecha = teclat.nextLine();
                    
                    Conte cuento = new Conte(category, num, codi, nom, editorial, new Date());
                    contesArraylist.add(cuento);
                    break;


                case 2:
                    System.out.println("Conte to HasMap");
                    break;

                case 3:
                    break;
            }

        } while (option != 3);

    }

}
