/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;
import java.util.Date;
/**
 *
 * @author roy
 */
public class Revista extends Publicacio {
    private int issn;
    private int numero;
    private int periodicidad;//Meses
    
    
    //Default constructor
    public Revista(String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
    }

    //Full Constructor 
    public Revista(String codi, String nom, String editorial, Date data, int issn, int numero, int periodicidad) {
        super(codi, nom, editorial, data);
        this.issn = issn;
        this.numero = numero;
        this.periodicidad = periodicidad;
    }

    //GETTERS AND SETTERS
    public int getPeriodicidad() {
        return periodicidad;
    }

    public void setPeriodicidad(int periodicidad) {
        this.periodicidad = periodicidad;
    }

    public int getIssn() {
        return issn;
    }

    public void setIssn(int issn) {
        this.issn = issn;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    //toString
    @Override
    public String toString() {
        return "Revista{" + "issn=" + issn + ", numero=" + numero + ", periodicidad=" + periodicidad + '}';
    }

    //equals
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Revista other = (Revista) obj;
        if (this.issn != other.issn) {
            return false;
        }
        if (this.numero != other.numero) {
            return false;
        }
        if (this.periodicidad != other.periodicidad) {
            return false;
        }
        return true;
    }
}
