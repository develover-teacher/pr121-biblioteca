/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;

/**
 *
 * @author vican
 */
public class cintesVideo extends Publicacio {
    
        double duracio;
	private String formatVHSBetaMax;

    public cintesVideo(String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
    }

    public double getDuracio() {
        return duracio;
    }

    public void setDuracio(double duracio) {
        this.duracio = duracio;
    }

    public String getFormatVHSBetaMax() {
        return formatVHSBetaMax;
    }

    public void setFormatVHSBetaMax(String formatVHSBetaMax) {
        this.formatVHSBetaMax = formatVHSBetaMax;
    }
    
    
    
    
}
