/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;

/**
 *
 * @author tarda
 */
public class Disquet extends Publicacio {
    
    private String tipusDisquet;
    
    public Disquet(String codi, String nom, String editorial, Date data, String tipusDisquet) {
        super(codi, nom, editorial, data);
        this.tipusDisquet=tipusDisquet;
    }

    public String getTipusDisquet() {
        return tipusDisquet;
    }

    public void setTipusDisquet(String tipusDisquet) {
        this.tipusDisquet = tipusDisquet;
    }
    /**
     * Funcio creada per fer el tercer punt de la part dos de la practica
     * @return Retorna un estrig que diu que ho he modificat jo(Francisco Regaña)
     */
    public String modificatPerFranciscoReganya(){
        return "Això ho ha modificat el Francisco Regaña";
    }
    
    public void disquetEspecial(String tipydisquet){
        if(tipydisquet.equals(this.tipusDisquet) ){
            System.out.println("Disquet especial A");
        }else{
            System.out.println("No es un disquet especial");
        }
    }
}
