/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;

/**
 *
 * @author Albert
 */
public class Videojocs extends Publicacio {
    private String genere;
    private String dificultat;

    public Videojocs(String genere, String dificultat, String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
        this.genere = genere;
        this.dificultat = dificultat;
    }

    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }
    
    public String getDificultat() {
        return dificultat;
    }

    public void setDificultat(String dificultat) {
        this.dificultat = dificultat;
    }
    
    @Override
    public Date getData() {
        return super.getData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setData(Date data) {
        super.setData(data); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getEditorial() {
        return super.getEditorial(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setEditorial(String editorial) {
        super.setEditorial(editorial); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getNom() {
        return super.getNom(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setNom(String nom) {
        super.setNom(nom); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCodi() {
        return super.getCodi(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCodi(String codi) {
        super.setCodi(codi); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Videojocs{" + "genere=" + genere + ", dificultat=" + dificultat + '}';
    }
    
}
