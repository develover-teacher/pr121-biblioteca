package biblioteca;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * @author mique
 */
public class ProgramaPublicacions {
    
    static String[] menuProva = {
	// "ET TROBES AL MENÚ PRINCIPAL DEL ADMINISTRADOR.",
	"Afegir nova publicació.",
	"Consultar publicació per codi.",
        "Afegir nou llibre.",
        "Afegir nova revista.",
        "Afegir nou diari.",
        "Afegir nou videojoc.",
        "Afegir nou ebook.",
        "Afegir nou conte.",
        "Afegir nou disc òptic(CD o DVD).",
	"Sortir."
    };
    
    static Scanner teclat = new Scanner(System.in);
        
    public static String llegeixParametre(String nomParam) {
        System.out.println("Introdueix el paràmete " + nomParam);
        String param = teclat.nextLine();
        return param;
    }
    
    public static Publicacio creaPublicacioNova() throws ParseException {
        String codPubNou = llegeixParametre("Codi publicació");
        String nomPubNou = llegeixParametre("Nom publicació");
        String editorial = llegeixParametre("Editorial");
        String dataPubStr = llegeixParametre("Data Publicacio dd/mm/aaaa");
        Date dataPubFormatted;
        if(!dataPubStr.matches("^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$"))
        {
            dataPubFormatted = new Date();
        } else {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            dataPubFormatted = formatter.parse(dataPubStr);
        }
        return new Publicacio(codPubNou,nomPubNou,editorial,new Date());
    }
    
    public static void main(String[] args) throws ParseException {
                
	Menu menu = new Menu(menuProva);
        ColPublicacions publicacions = new ColPublicacions();
        boolean dentroMenu = true;
        Publicacio pubNova; 
        
        while(dentroMenu) {   
            menu.mostraMenuPantalla();
            int opcioMenu = menu.triaOpcioMenu();  
            switch (opcioMenu) {
                case 1:
                    System.out.println("");
                    pubNova = creaPublicacioNova();
                    publicacions.creaPublicacio(pubNova);
                    break;
                case 2:
                    String codPub = llegeixParametre("Codi publicació");
                    System.out.println(publicacions.mostraPublicacioPerCodi(codPub));
                case 3:
                    // TODO "Afegir nou llibre.",
                    System.out.println("Afegir nou llibre.");
                    pubNova = creaPublicacioNova();
                    // params llibre
                    // Llibre ll = new Llibre();
                case 4:
                    // TODO "Afegir nova revista.",
                    System.out.println("Afegir nova revista.");
                     // params revista
                case 5:
                    // TODO "Afegir nou diari."
                     System.out.println("Afegir nou diari.");
                case 6:
                    // TODO  "Afegir nou videojoc.",
                    System.out.println( "Afegir nou videojoc.");
                case 7:
                    // TODO "Afegir nou ebook.",
                    System.out.println("Afegir nou ebook.");
                case 8:
                    //  TODO "Afegir nou conte.",
                    System.out.println("Afegir nou conte.");
                case 9:
                    //  "Afegir nou disc òptic(CD o DVD).",
                    System.out.println("Afegir nou disc òptic(CD o DVD).");
                case 10:
                    dentroMenu=false;
            }
        }
        
    }
}
