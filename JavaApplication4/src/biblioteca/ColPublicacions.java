/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mique
 */
public class ColPublicacions {
 
    Map<String,Publicacio> publicacions;
    
    ColPublicacions() {
        publicacions = new HashMap<>();
    }
    
    public void creaPublicacio(Publicacio p) {
       publicacions.put(p.getCodi(),p);
    }
    
    public void eliminaPublicacio(Publicacio p) {
       publicacions.remove(p.getCodi(),p);
    }
    
    public void editaPublicacio(Publicacio novaPub) {
       publicacions.replace(novaPub.getCodi(),novaPub);
    }
    
    public void llistaPublicacions() {
        for (String clauPub : publicacions.keySet()) {
            Publicacio pub = publicacions.get(clauPub);
            System.out.println(clauPub + " " + pub);  
        }
    }
    
    public Publicacio mostraPublicacioPerCodi(String codi) {
       return publicacions.get(codi);
    }
    
    public static void main(String[] args) {
        System.out.println("TEST COLECCIO PUBLICACIONS");
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018,11,23);
        Publicacio pub = new Publicacio("L-","Feminisme de butxaca","Bel Olid", calendar.getTime());
    }
}
