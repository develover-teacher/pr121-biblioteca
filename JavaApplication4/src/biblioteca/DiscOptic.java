/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author jimmy y steven
 */
public class DiscOptic extends Publicacio{
    
    private String codigo;
    private String format;
    private int numeroExemplars;

    public DiscOptic(String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
    }

    public DiscOptic(String codigo, String format, int numeroExemplars, String codi, String nom, String editorial, Date data) {
        super(codi, nom, editorial, data);
        this.codigo = codi;
        this.format = format;
        this.numeroExemplars = numeroExemplars;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getNumeroExemplars() {
        return numeroExemplars;
    }

    public void setNumeroExemplars(int numeroExemplars) {
        this.numeroExemplars = numeroExemplars;
    }

    @Override
    public String toString() {
        return "DiscOptic{" + "codigo=" + codigo + ", format=" + format + ", numeroExemplars=" + numeroExemplars + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DiscOptic other = (DiscOptic) obj;
        if (this.numeroExemplars != other.numeroExemplars) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.format, other.format)) {
            return false;
        }
        return true;
    }
    
    
}
