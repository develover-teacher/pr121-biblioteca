package biblioteca;

/**
 * @author mique
 */
public class ExempleMenu_1 {
    
    static String[] menuProva = {
	// "ET TROBES AL MENÚ PRINCIPAL DEL ADMINISTRADOR.",
	"Afegir publicació.",
	"Consultar publicació per codi.",
	"Sortir."
    };
        
    public static void main(String[] args) {
        
	Menu menu = new Menu(menuProva);
        boolean dentroMenu = true;
        
        while(dentroMenu) {   
            menu.mostraMenuPantalla();
            int opcioMenu = menu.triaOpcioMenu();  
            switch (opcioMenu) {
                case 1:
                    System.out.println("Afegint publicació.");
                    break;
                case 2:
                    System.out.println("Consultant publicació per codi.");
                    break;
                case 3:
                    dentroMenu=false;
            }
        }
        
    }
}
