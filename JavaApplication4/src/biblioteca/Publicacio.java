package biblioteca;

import java.util.Date;

/**
 * @author Miquel Àngel Amorós Alberich
 */
public class Publicacio 
{
  String codi;
  private String nom;
  private String editorial;
  private Date data;

  public Publicacio(String codi, String nom, String editorial, Date data) 
  {  
  	this.codi = codi;
  	this.nom = nom;
  	this.editorial = editorial;
  	this.data = data;
  }

  public void setCodi(String codi) {
	this.codi = codi;
  }
  public String getCodi() {
	return codi;
  }
  public void setNom(String nom) {
	this.nom = nom;
  }
  public String getNom() {
	return nom;
  }
  public void setEditorial(String editorial) {
	this.editorial = editorial;
  }
  public String getEditorial() {
	return editorial;
  }
  public void setData(Date data) {
	this.data = data;
  }
  public Date getData() {
	return data;
  }

    @Override
    public String toString() {
        return "Publicacio{" + "codi=" + codi + ", nom=" + nom + ", editorial=" + editorial + ", data=" + data + '}';
    }
 
  
}
