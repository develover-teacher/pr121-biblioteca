package readwritefile;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Exemple de com llegir un fitxer de text en Java
 * @author develover
 */
public class ReadUsersFile {
    
    public static void main(String[] args) {
        
        FileInputStream fstream = null;
        try {
            boolean trobat = false;
            String usuari = "username";
            String contrasenya = "123456";
            //vull obrir un arxiu de texte d'usuaris en mode lectura
            String ruta = "";
            fstream = new FileInputStream("users.txt");
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            // Llegeix el fitxer linea per linea.
            while ((strLine = br.readLine()) != null) {
                String n = strLine.split(":")[0];
                String c = strLine.split(":")[1];
                System.out.println(n + " " + c);
                System.out.println(c.equals(contrasenya));
                if (n.equals(usuari) && c.equals(contrasenya)) {
                    trobat = true ;
                    break;
                }
            }
            if(trobat) {
                System.out.println("Usuari " + usuari + " trobat!");
            } else {
                System.out.println("No s'ha trobat l'usuari " + usuari);
            }
            in.close();
            fstream.close();
        } catch (Exception ex) {
            Logger.getLogger(ReadUsersFile.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
