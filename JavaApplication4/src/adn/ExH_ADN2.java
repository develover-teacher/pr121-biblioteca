package adn;

import java.util.Arrays;
import java.util.Scanner;

/**
 * h. Donada una tira d’ADN introduïda per teclat, programa que dóna la volta
 * a la tira i la mostra per pantalla.
 * @author alumne
 */
public class ExH_ADN2 {
  
  public static void main(String[] args) {
      Scanner entrada = new Scanner(System.in);
      // String exempleADN = "ATGCGTAT";
      System.out.println("Escribe una cadena de ADN:");
      String exempleADN = entrada.nextLine();
      // Sol1.
      char[] caractersADN = exempleADN.toCharArray(); 
      int ultimaPos = caractersADN.length;
      String resultat = "";
      for (int i = ultimaPos-1; i >= 0; i--) {
          resultat+=caractersADN[i];
      }
      System.out.println(resultat);
      // Sol2.
      // System.out.println(new StringBuilder(exempleADN).reverse().toString());
    } 
}
