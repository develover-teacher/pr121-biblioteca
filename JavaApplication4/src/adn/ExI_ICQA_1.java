package adn;

import java.util.Arrays;

/**
 * i. Agafem 2 arrays amb números que representen l’índex de qualitat de l’aire 
 * del 2016 i 2017 en 10 ciutats catalanes.
 * Els valors van de -100(fatal) a +100(genial). 
 * Més info: https://www.idescat.cat/pub/?id=aec&n=236&lang=es&t=2016
 * Crea un nou array que calculi la diferència entre els valors del 2016 i el 2017. 
 * Mostra els valors positius de la nova taula.
 * @author alumne
 */
public class ExI_ICQA_1 {
  
  public static void main(String[] args) {
        
      int[] dades_2016 = {56,54,61,45,66,42,59,71,49,60};
      int[] dades_2017 = {59,53,65,42,69,42,55,72,53,57};
      Integer[] comparativa_16_17 = new Integer[dades_2017.length];
      
      System.out.println("ExI_ICQA. Comparativa 2016-2017:");
      int valorComp;
      for (int i = 0; i < dades_2016.length; i++) {
          valorComp = dades_2017[i] - dades_2016[i];
          comparativa_16_17[i] = valorComp;
      }
//      System.out.println(Arrays.toString(comparativa_16_17));
      System.out.println("Valors positius.");
      for (Integer valorComp1617 : comparativa_16_17) {
          if(valorComp1617>0)
            System.out.println(valorComp1617);
      }
      
    } 
}
