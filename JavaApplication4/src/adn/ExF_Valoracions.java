/*
 * f. Escriure una aplicació per a analitzar les valoracions de l’1 al 5 de 15 clients. 
 * Crea un array amb 15 valoracions de l’1 al 5.
 * Calcular la mitjana de tothom (en decimal) i el número de clients que ha posat
 * cada valoració. Per exemple: 
 * 5 * = 2
 * 4 * = 3
 */
package adn;

public class ExF_Valoracions {
    public static void main(String[] args) {
        // Valoracions ficticies.
        int[] valoracions = new int[] {4,3,3,5,4,2,1,3,5,4,3,4,5,4};
        double total = 0, mitjanaVal = 0;
        
        int[] numClientsValoracio = new int[5];
        
        // Agrupar valoracions. 
        for (int valoracio : valoracions) {
            numClientsValoracio[valoracio-1]++;
            total = total + valoracio;
        }
        mitjanaVal = total / (double) valoracions.length;
        
        System.out.println("total="+total);
        System.out.println("mitjanaVal="+mitjanaVal);
        
        int cont = 1;
        for (int valor : numClientsValoracio) {
            System.out.println(cont + " * = " + valor);
            cont++;
        }
        System.out.println("mitjanaVal="+mitjanaVal);
    }
}
