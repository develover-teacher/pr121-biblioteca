package adn;

import java.util.Arrays;
import java.util.Scanner;

/**
 * g. Aplicació que calcula el total de cadascuna de de les seves bases: A, G, T
 * i de les C en una tira d’ADN introduïda prèviament per teclat. Al final se’ns
 * mostra els parcials de cada base i la tira original però canviant les T per
 * U.
 *
 * @author alumne
 */
public class ExG_ADN1 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        // String exempleADN = "ATGCGTAT";
        System.out.println("Escribe una cadena de ADN:");
        String exempleADN = entrada.nextLine();
        
        char[] caractersADN = exempleADN.toCharArray();
        
        int ContA=0,ContT=0,ContG=0,ContC = 0;
        
        for (char c : caractersADN) {
            switch (c) {
                case 'A':
                    ContA++;
                    break;
                case 'T':
                    ContT++;
                    break;
                case 'G':
                    ContG++;
                    break;
                case 'C':
                    ContC++;
                    break;
                default:
                    System.out.println("Error. Secuencia ADN no válida.");
                    System.exit(0);
                    break;
            }
        }
        System.out.println("ContA="+ContA);
        System.out.println("ContT="+ContT);
        System.out.println("ContG="+ContG);
        System.out.println("ContC="+ContC);
    }
}
