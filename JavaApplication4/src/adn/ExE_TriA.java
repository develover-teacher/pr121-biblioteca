package adn;

import java.util.Arrays;
import java.util.Scanner;

/**
 * e. Dibuiza una piramide d’N asteriscs per pantalla, on N és un número major
 * a 0 passat per teclat.
 * @author alumne
 */
public class ExE_TriA {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        // String exempleADN = "ATGCGTAT";
        System.out.println("Escribe un número mayor a 0:");
        int N = entrada.nextInt();
        String linea = "";
        for (int i = 1; i <= N; i++) {
            linea+="*";
            System.out.println(linea);
        }
        System.out.println();
        // Piràmide inversa
        for (int i = N; i >= 0; i++) {
            for (int j = 0; j < i; j++) {
                linea+="*";
            }
            System.out.println(linea);
            linea="";
        }
    }
}
